'use strict';


module.exports = (sequelize, DataTypes) => { 
  /**
    * Define the user table & associate it
    * with the bucket & idea table
    */
  var user = sequelize.define('user',
    {
      id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true }
      , username: { type: DataTypes.STRING }
      , first:    { type: DataTypes.STRING }
      , last:     { type: DataTypes.STRING }
    } //2nd param
    , {
      classMethods: {
        associate: (models) => {
          user.hasMany(models.bucket);
          user.hasMany(models.idea);
        }
      }
    } //3rd param
  );

  return user;
};
