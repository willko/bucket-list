'use strict';


module.exports = (sequelize, DataTypes) => {
  /**
    * Define the bucket table & associate it
    * with the idea & user tables
    */
  var bucket = sequelize.define('bucket',
    {
      id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
      title: { type: DataTypes.STRING },
      length: { type: DataTypes.INTEGER }
    }, //2nd param
    {
      classMethods: {
        associate: (models) => {
          bucket.hasMany(models.idea);
          bucket.belongsTo(models.user, {foreignKey: 'userId' });
        }
      }
    } //3rd param
  );

  return bucket;
};
