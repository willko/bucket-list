'use strict';


module.exports = (sequelize, DataTypes) => { 
  /**
    * Define the idea table & associate it
    * with the bucket & user tables
    */
  var idea = sequelize.define('idea',
    {
      id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
      entry: { type: DataTypes.STRING },
      state: { type: DataTypes.STRING }
    }, //2nd param
    {
      classMethods: {
        associate: (models) => {
          idea.belongsTo(models.bucket, { foreignKey: 'bucketId' });
          idea.belongsTo(models.user, { foreignKey: 'userId' });
        }
      }
    } //3rd param
  );

  return idea;
};
