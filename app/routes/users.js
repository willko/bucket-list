'use strict';

var express = require('express')
  , router = express.Router()
  , models  = require('../models')
  , mw = require('./middleware').user;


/* GET users listing. */
router.get('/', mw.getAllUsers, (req, res) => {
    res.render('users/users', {
      title: 'Users',
      users: req.users
    });
});


/* GET user creation view. */
router.get('/new', (req, res) => {
  res.render('users/create-user', {
    title: 'New User'
  });
});

/* POST create new user. */
router.post('/new', (req, res) => {
 let usr = req.body;
  models.user.create({
      username: usr.username
    , first: usr.first
    , last: usr.last
  }).then((users) => {
    res.render('users/create-user-success', {
      title: 'User Successfully Created',
      users: users
    });
  });
});

/* GET user by id. */
router.get('/:userId', mw.getUser, (req, res) => {
  res.send(req.user);
});

/* GET user profile. */
router.get('/:userId/profile', mw.getUser, (req, res) => {
  res.render('users/profile', {
      title: 'User Profile'
    , user: req.user
  });
});

module.exports = router;
