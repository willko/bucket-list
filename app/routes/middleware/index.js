'use strict';

var user_middleware = require('./user-mw.js');
var bucket_middleware = require('./bucket-mw.js');


module.exports = {
    user: user_middleware
  , bucket: bucket_middleware
};
