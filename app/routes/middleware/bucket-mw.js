'use strict';

var models = require('../../models');

module.exports = {
  // attaches all buckets in db
  getAllBuckets: (req, res, next) => {
    models.bucket.findAll()
      .then((buckets) => {
        req.buckets = buckets;
      }).then(() => {
        next();
      });
  }
  // attaches buckets with matching userId
  , getUserBuckets: (req, res, next) => {
    models.bucket.findAll({
      where: {userId: {$eq: req.params.userId}}
    }).then((buckets) => {
      req.buckets = buckets;
    }).then(() => {
      next();
    });
  }
  // attaches bucket matching bucketId
  , getBucket: (req, res, next) => {
    models.bucket.findOne({
      where: {id: {$eq: req.params.bucketId}},
      include: [models.idea]
    }).then((bucket) => {
      req.bucket = bucket;
    }).then(() => {
      next();
    });
  }
};
