'use strict';

var models = require('../../models');

module.exports = {
  //attaches current user defined by :userId route param
  getUser: (req, res, next) => {
    models.user.findOne({
      where: { id: { $eq: req.params.userId }}
    }).then( (user) => {
      req.user = user;
    }).then(() => { next(); });
  }
  //attaches all users in db
  , getAllUsers: (req, res, next) => {
    models.user.findAll({
      include: [ models.idea, models.bucket ]
    }).then((users) => {
      req.users = users;
    }).then(() => { next(); });
  }
};
